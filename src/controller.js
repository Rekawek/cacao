var board = require( './board.js' )();
var view = require( './view.js' )();


var tileOnHand;
var rotation;

window.board = board;

var printBoardTiles =  function printBoardTiles() {
    return view.printTiles( board.tiles );
};

var showPossibleMovesVillige = function() {
    printWithActions( board.possibleMoves(), villigeClick );
};

var showPossibleMovesJungle = function( ) {
    printWithActions( board.possibleJungleMoves(), jungleClick );
};

var checkMovesLeft = function checkMovesLeft() {

    view.clear();
    printBoardTiles();

    if ( board.possibleJungleMoves().length > 0 ) {
        showJungleHand();
    } else {
        window.gameLoop.nextStep();
    }
};

var showHand = function showHand() {
    var hand = board.players.current().tilesOnHand();
    for ( var i = 0; i < hand.length; i++ ) {
        var data = { x: i,
            y: 0,
            action: handClick( i ),
            color: board.players.current().color,
            name: hand[ i ],
            rotation: 0
          };
        view.addInteractiveTile( data, 'hand' );
    }
};

var showJungleHand = function showJungleHand() {
    var hand = board.jungleTilesHand();
    for ( var i = 0; i < hand.length; i++ ) {
        var data = { x: i,
            y: 0,
            action: jungleHandClick( i ),
            color: null,
            name: hand[ i ],
            rotation: 0
          };
        view.addInteractiveTile( data, 'hand' );
    }
};

var showRotations = function showRotations() {

    for ( var i = 0; i < 4; i++ ) {
        var data = { x: 0,
            y: i,
            action: rotatedTileClick( i ),
            color: board.players.current().color,
            name: tileOnHand,
            rotation: i
          };

        view.addInteractiveTile( data, 'options' );
    }
};

var triggerActions = function triggerActions() {

    var tilesToTrigger = board.tilesAddedThisTurn();
    console.log( 'tiles to trigger', tilesToTrigger );

    for ( var key in tilesToTrigger ) {
        view.addTileAction( tilesToTrigger[ key ],  tileActionClick( tilesToTrigger[ key ] ) );
    }

    //window.gameLoop.nextStep( 'triggerActions' );
};

var gameLoop = function gameLoop() {
    var i = 0;
    var steps = [
        autoStep( view.clear ),
        autoStep( printBoardTiles ),
        showHand,
        showRotations,
        showPossibleMovesVillige,
        autoStep( view.clearHand ),
        autoStep( view.clearOptions ),
        checkMovesLeft,
        triggerActions,
        autoStep( board.players.next )
    ];

    var stepsName = [
        'clear',
        'printBoardTiles',
        'showHand',
        'showRotations',
        'showPossibleMovesVillige',
        'clearHand',
        'clearOptions',
        'checkMovesLeft',
        'triggerActions',
        'next'
    ];


    var nextStep = function nextStep() {
        if ( i < steps.length ) {
            i++;
            steps[ i - 1 ]();
        } else {
            i = 0;
            steps[ i ]();
        }
    };

    var nextStepByName = function nextStepByName( currentStepname ) {

        i = stepsName.indexOf( currentStepname ) + 1;
        if ( i < 0 ) {
            console.log( 'error step not found' );
        }
        console.log( 'going to step', i );
        nextStep();
    };

    var sameStep = function sameStep() {
        steps[ i - 1 ]();
    };

    function autoStep ( runMe ) {
        return function( ) {
            runMe();
            nextStep();
        };
    }
    var start = function start() {
        board.clearTilesFromThisTurn();
        steps[ 0 ]();
    };

    return {
        start: start,
        nextStep:nextStep,
        sameStep: sameStep,
        nextStepByName: nextStepByName
    };
};

window.gameLoop = gameLoop();

var startGame = function startGame() {
    board.players.addMe(  'busz', 'r' );
    board.players.addMe(  'pat', 'p' );

    board.addTile( 0, 0, 'j', 'market_2' );
    board.addTile( 1, 1, 'j', 'cacao_1' );
    window.gameLoop.start();
};

var villigeClick = function( x, y ) {
    return function() {
        board.addVilligeTile( x, y, tileOnHand, rotation );
        board.players.current().removeTileFromHand( tileOnHand );
        window.gameLoop.nextStepByName( 'showPossibleMovesVillige' );
    };
};

var handClick = function( i ) {
    return function() {
        tileOnHand = board.players.current().pickFromHand( i );
        window.gameLoop.nextStepByName( 'showHand' );
    };
};

var jungleHandClick = function( i ) {
    return function() {
        tileOnHand = board.pickFromHand( i );
        showPossibleMovesJungle();
    };
};

var rotatedTileClick = function( i ) {
    return function() {
        rotation = i;
        window.gameLoop.nextStep( 'showRotations' );
    };
};

var jungleClick = function( x, y ) {
    return function() {
        board.addJungleTile( x, y, tileOnHand );
        board.removeTileFromHand( tileOnHand );
        window.gameLoop.sameStep();
    };
};

var tileActionClick = function ( tile ) {
    return function() {
        console.log( 'clicked tile:', tile );
    };
};

var printWithActions = function( array, clickFunction ) {
    for ( var i = 0; i < array.length; i++ ) {
        var tile = array[ i ];
        var x = tile.x;
        var y = tile.y;
        view.addInteractiveElement( x, y, clickFunction( x, y ), '#000' );
    }
};

window.startGame = startGame;
