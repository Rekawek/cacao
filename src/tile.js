var _ = require( 'lodash' );

module.exports = function tile() {

    var rotation = 0;
    var _type = '';
    var types = {
        workers_1: {
            t: 1,
            r: 1,
            b: 1,
            l: 1
        },
        workers_2: {
            t: 0,
            r: 1,
            b: 2,
            l: 1
        },
        workers_3_1: {
            t:0,
            r:0,
            b:3,
            l:1
        },
        workers_3_2: {
            t:0,
            r:1,
            b:3,
            l:0
        }
    };

    var workers = {
        t: 0,
        r: 0,
        b: 0,
        l: 0
    };

    var _name = '';

    var setType = function setType( type ) {
        _type = type;
    };
    var getType = function getType() {
        return _type;
    };

    var getName = function getName() {
        return _name;
    };

    var setName = function setName( name ) {

        _name = name;
        if ( _type === 'v' ) { //only villige tiles have workers
            workers = _.clone( types[ name ] ); //this is obsolete? propably...
        }
    };

    var getRotation = function rotationGet() {
        return rotation;
    };

    var setRotation = function rotationSet( n ) {
        var cloneWorkers = [ workers.t, workers.r, workers.b, workers.l ];
        workers.t = cloneWorkers[ ( 0 + n ) % 4 ];
        workers.r = cloneWorkers[ ( 1 + n ) % 4 ];
        workers.b = cloneWorkers[ ( 2 + n ) % 4 ];
        workers.l = cloneWorkers[ ( 3 + n ) % 4 ];
        console.log( 'poRotacji', workers );
        rotation = n;
    };

    var emptyEdges = function() {
        var fields = [];
        if ( this.t === null ) {
            fields.push( { x: this.x, y: this.y - 1 } );
        }

        if ( this.b === null ) {
            fields.push( { x: this.x, y: this.y + 1 } );
        }

        if ( this.r === null ) {
            fields.push( { x: this.x + 1, y: this.y } );
        }

        if ( this.l === null ) {
            fields.push( { x: this.x - 1, y: this.y } );
        }
        return fields;
    };

    var emptyWorkers = function() {

        var fields = [];
        if ( this.t === null && workers.t > 0 ) {
            fields.push( { x: this.x, y: this.y - 1 } );
        }

        if ( this.b === null && workers.b > 0 ) {
            fields.push( { x: this.x, y: this.y + 1 } );
        }

        if ( this.r === null && workers.r > 0 ) {
            fields.push( { x: this.x + 1, y: this.y } );
        }

        if ( this.l === null && workers.l > 0 ) {
            fields.push( { x: this.x - 1, y: this.y } );
        }
        return fields;
    };

    return {
        t:null,
        r:null,
        b:null,
        l:null,
        type:_type,
        name: _name,
        x: null,
        y: null,
        setName: setName,
        getName: getName,
        getRotation: getRotation,
        setRotation: setRotation,
        workers: workers,
        emptyEdges: emptyEdges,
        emptyWorkers: emptyWorkers,
        setType: setType,
        getType: getType
    };
};
