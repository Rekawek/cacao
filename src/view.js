

module.exports = function view( ) {

    var divTile = function( tile ) {
        var newDiv = document.createElement( 'div' );
        newDiv.style.position = 'absolute';
        newDiv.style.width = '100px';
        newDiv.style.height = '100px';
        newDiv.style.left = 100 * tile.x + 'px';
        newDiv.style.top =  100 * tile.y + 'px';
        newDiv.style.backgroundSize = '100% 100%';

        if ( tile.color ) {
            tile.color = '_' + tile.color;
        } else {
            tile.color = '';
        }
        if ( typeof tile.getName === 'function' ) {
            newDiv.style.backgroundImage = 'url("img/' + tile.getName() + tile.color + '.jpg")';
        }
        else if ( tile.name && tile.name.length > 0 ) {
            newDiv.style.backgroundImage = 'url("img/' + tile.name + tile.color + '.jpg")';
        }

        if ( typeof tile.getRotation === 'function' ) {
            newDiv.style.transform = 'rotate(' + tile.getRotation() * -90 + 'deg)';
        }
        else if ( tile.rotation && typeof tile.rotation === 'number' ) {
            newDiv.style.transform = 'rotate(' + tile.rotation * -90 + 'deg)';
        }

        return newDiv;
    }  ;

    var divElement = function( x, y ) {
        var newDiv = document.createElement( 'div' );
        newDiv.style.position = 'absolute';
        newDiv.style.width = '100px';
        newDiv.style.height = '100px';
        newDiv.style.left = 100 * x + 'px';
        newDiv.style.top =  100 * y + 'px';
        newDiv.style.backgroundColor= '#000';
        return newDiv;
    }  ;

    var addToGame = function( element ) {
        document.querySelector( '#game' ).appendChild( element );
    };

    var addToHand = function( element ) {
        document.querySelector( '#hand' ).appendChild( element );
    };

    var addToOptions = function( element ) {
        document.querySelector( '#options' ).appendChild( element );
    };

    var clear = function() {
        var myNode = document.querySelector( '#game' );
        while ( myNode.firstChild ) {
            myNode.removeChild( myNode.firstChild );
        }
    };

    var clearHand = function clearHand() {
        var myNode = document.querySelector( '#hand' );
        while ( myNode.firstChild ) {
            myNode.removeChild( myNode.firstChild );
        }
    };

    var clearOptions = function clearHand() {
        var myNode = document.querySelector( '#options' );
        while ( myNode.firstChild ) {
            myNode.removeChild( myNode.firstChild );
        }
    };

    var printTile = function( tile ) {
        if ( tile.player && tile.player.color ) {
            tile.color = tile.player.color;
        }

        var newDiv = divTile( tile );
        newDiv.id= 'x' + tile.x + 'y' + tile.y;
        document.querySelector( '#game' ).appendChild( newDiv );
    };

    var printTiles = function( array ) {
        for ( var i = 0; i < array.length; i++ ) {
            var tile = array[ i ];
            var x = tile.x;
            var y = tile.y;
            if ( tile.getType() === 'j' ) {
                tile.color = null;
                printTile( tile );
            } else {
                printTile( tile );
            }
        }
    };

    //{x,y,action,color,name,rotation}, where
    var addInteractiveTile = function( data, where ) {
        console.log( 'interTile', data );
        var newDiv = divTile( data );
        newDiv.onclick = data.action;
        if ( where === 'game' ) {
            addToGame( newDiv );
        } else if ( where === 'hand' )  {
            addToHand( newDiv );
        } else if ( where === 'options' ) {
            addToOptions( newDiv );
        } else {
            console.log( 'addInteractiveTile where not defined' );
        }
    };

    var addInteractiveElement = function( x, y, action, color ) {
        var newDiv = divElement( x, y );
        newDiv.style.backgroundColor = color;
        newDiv.onclick = action;
        addToGame( newDiv );
    };
    var selectTile = function selectTile( tile ) {
        return document.getElementById('x' + tile.x + 'y' + tile.y );
    };

    var addTileAction = function( tile, action ) {
        var selector = selectTile( tile );
        selector.style.border =  '1px solid red';
        selector.onclick = action;
    };

    var removeTileAction = function( tile ) {
        selectTile( tile ).onClick = "";
    };

    return {
        printTiles: printTiles,
        clear:clear,
        clearHand:clearHand,
        clearOptions: clearOptions,
        addToOptions: addToOptions,
        addToHand: addToHand,
        addElement: addToGame,
        addTileAction: addTileAction,
        removeTileAction: removeTileAction,
        addInteractiveElement: addInteractiveElement,
        addInteractiveTile: addInteractiveTile
    };

};
