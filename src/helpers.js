module.exports = function helpers() {
    var shuffle = function shuffle( b ) {
        a = b.slice();
        var j, x, i;
        for ( i = a.length; i; i -= 1 ) {
            j = Math.floor( Math.random() * i );
            x = a[ i - 1 ];
            a[ i - 1 ] = a[ j ];
            a[ j ] = x;
        }
        return a;
    };

    var takeTilesToHand =  function takeTilesToHand( tiles,hand, n ) {
        while ( hand.length !== n ) {
            var tile = tiles.pop();
            if ( tile === undefined ) {
                break;
            }
            hand.push( tile );
        }
        return hand;
    };

    return {
        shuffle: shuffle,
        takeTilesToHand: takeTilesToHand
    };

}();
