var helpers = require( './helpers.js' );

var fillArrayWithValue = require( './fillArray' );

var _player = function( name, color ) {
    name = name || 'brutus';
    color = color || 'r';
    var _hand = [];

    var _createTiles = function createTiles() {
        var arr = [];
        fillArrayWithValue( arr, 4, 'workers_1' );
        fillArrayWithValue( arr, 5, 'workers_2' );
        fillArrayWithValue( arr, 1, 'workers_3_1' );
        fillArrayWithValue( arr, 1, 'workers_3_2' );

        arr = helpers.shuffle( arr );
        return arr;
    };
    var tilesOnHand = function tilesOnHand() {
        if ( _hand.length === 3 ) {
            return _hand;
        } else {
            _hand = helpers.takeTilesToHand( tiles, _hand, 3 );
            return _hand;
        }
    };

    var pickFromHand = function pickFromHand( i ) {
        var tile = _hand[ i ];
        return tile;
    };

    var tiles = _createTiles();

    var removeTileFromHand = function removeTileFromHand( name ) {
        if ( _hand.indexOf( name ) < 0 ) {
            console.log( 'tile dosent exist error' );
        }
        _hand.splice( _hand.indexOf( name ) , 1 );
    };

    return {
        color: color,
        name: name,
        tiles: tiles,
        tilesOnHand: tilesOnHand,
        pickFromHand: pickFromHand,
        removeTileFromHand: removeTileFromHand
    };
};

module.exports = function players() {

    var i = 0;
    var list = [];
    var addMe = function addplayer( name, color ) {
        list.push( new _player( name, color ) );
    };

    var next = function() {
        i++;
        if ( i > list.length - 1 ) {
            i = 0;
        }
    };

    var current = function() {
        return list[ i ];
    };

    return {
        next: next,
        addMe: addMe,
        current: current
    };

};
