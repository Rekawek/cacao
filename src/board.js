
var helpers = require( './helpers' );

module.exports = function board() {

    var tile = require( './tile.js' );

    var jungleTiles = [];

    var tilesAddedInCurrentTurn = [];

    var fillArrayWithValue = require( './fillArray.js' );
    fillArrayWithValue( jungleTiles, 5, 'cacao_1' );
    fillArrayWithValue( jungleTiles, 6, 'market_2' );
    fillArrayWithValue( jungleTiles, 2, 'cacao_2' );
    fillArrayWithValue( jungleTiles, 4, 'market_3' );
    fillArrayWithValue( jungleTiles, 1, 'market_4' );
    fillArrayWithValue( jungleTiles, 2, 'gold_1' );
    fillArrayWithValue( jungleTiles, 1, 'gold_2' );
    fillArrayWithValue( jungleTiles, 3, 'lake' );
    fillArrayWithValue( jungleTiles, 2, 'sun' );
    fillArrayWithValue( jungleTiles, 5, 'temple' );
    jungleTiles = helpers.shuffle( jungleTiles );

    var _players = require( './players.js' );

    var players = _players();

    var _tiles = [];

    var _connectTiles = function( tile, tile2 ) {
        var xDiff = tile.x - tile2.x;
        var yDiff = tile.y - tile2.y;
        if ( ( Math.abs( xDiff ) === 1 && yDiff === 0 ) ) {
            if ( xDiff > 0 ) {
                tile.l = tile2;
                tile2.r = tile;
            } else {
                tile.r = tile2;
                tile2.l = tile;
            }
        } else if ( Math.abs( yDiff ) === 1 && xDiff === 0 ) {
            if ( yDiff > 0 ) {
                tile.t = tile2;
                tile2.b = tile;
            } else {
                tile.b = tile2;
                tile2.t = tile;
            }
        } else {
            console.log( 'unable to connect tiles are not Adjecent' );
        }
    };

    var _updateConnections = function( newTile ) {
        var x = newTile.x;
        var y = newTile.y;
        for ( var i = 0; i < _tiles.length; i++ ) {
            var xDiff = _tiles[ i ].x - x;
            var yDiff = _tiles[ i ].y - y;

            //Check if the tiles are Adjecent
            if ( ( Math.abs( xDiff ) === 1 && yDiff === 0 ) || ( Math.abs( yDiff ) === 1 && xDiff === 0 ) ) {
                _connectTiles( _tiles[ i ], newTile );
            }
        }
    };

    var findTile = function( x, y ) {
        for ( var i = 0; i < _tiles.length; i++ ) {
            if ( _tiles[ i ].x === x && _tiles[ i ].y === y ) {
                return _tiles[ i ];
            }
        }
    };

    var adjecentTiles = function( x, y ) {
        var tile = findTile( x, y );
        var adjecent = [];
        if ( tile.t ) {
            adjecent.push( tile.t );
        }
        if ( tile.r ) {
            adjecent.push( tile.r );
        }
        if ( tile.b ) {
            adjecent.push( tile.b );
        }

        if ( tile.l ) {
            adjecent.push( tile.l );
        }
        return adjecent;
    };

    var addTile = function( x, y, type, name, rotation ) {
        var newTile = new tile();
        newTile.x = x;
        newTile.y = y;
        newTile.setType( type );
        newTile.setName( name );
        newTile.setRotation( rotation );
        console.log( 'dodawany tile', newTile );
        window.newTile = newTile;
        if ( type === 'v' ) {
            newTile.player = players.current();
        }

        _tiles.push( newTile );
        tilesAddedInCurrentTurn.push( newTile );
        _updateConnections( newTile );
        return newTile;
    };

    var addVilligeTile = function( x, y, name, rotation ) {
        var tile = addTile( x, y, 'v', name, rotation );
    };

    var addJungleTile = function( x, y, name ) {

        var tile = addTile( x, y, 'j', name );
    };

    var possibleJungleMoves = function() {

        var struct = {};
        var fieldsToJungle = [];

        for ( var i = 0; i < _tiles.length; i++ ) {
            if ( _tiles[ i ].getType() === 'v' ) {

                var edges = _tiles[ i ].emptyWorkers();

                //this is finding an empty field that have to adjecent fields with workers
                for ( var j = 0; j < edges.length; j++ ) {
                    if ( !struct[ edges[ j ].x ] ) {
                        struct[ edges[ j ].x ] = {};
                    }
                    if ( !struct[ edges[ j ].x ][ edges[ j ].y ] ) {
                        struct[ edges[ j ].x ][ edges[ j ].y ] = 1;
                    } else {
                        struct[ edges[ j ].x ][ edges[ j ].y ] += 1;
                    }
                    if ( struct[ edges[ j ].x ][ edges[ j ].y ] > 1 ) {
                        fieldsToJungle.push( { x: edges[ j ].x, y: edges[ j ].y } );
                    }
                }
            }
        }
        return fieldsToJungle;
    };

    var possibleMoves = function() {
        var possibleMoves = [];
        for ( var i = 0; i < _tiles.length; i++ ) {
            if ( _tiles[ i ].getType() === 'j' ) {
                possibleMoves = possibleMoves.concat( _tiles[ i ].emptyEdges() );
            }
        }
        if ( possibleMoves.length === 0 ) {
            console.log( 'error no possible moves' );
        }

        return possibleMoves;
    };

    var _table = [];
    var reveleadTiles = function() {
        if ( table.length === 2 ) {
            return table;
        } else {
            table = helpers.takeTilesToHand( jungleTiles, 2 );
            return table;
        }
    };

    var jungleHand = [];
    var jungleTilesHand = function jungleTilesHand() {
        jungleHand = helpers.takeTilesToHand( jungleTiles, jungleHand, 2 );
        return jungleHand;
    };

    var pickFromHand = function pickFromHand( i ) {
        var tile = jungleHand[ i ];
        return tile;
    };

    var removeTileFromHand = function removeTileFromHand( name ) {
        if ( jungleHand.indexOf( name ) < 0 ) {
            console.log( 'tile dosent exist error' );
        }
        jungleHand.splice( jungleHand.indexOf( name ) , 1 );
    };



    var _triggerActions = function _triggerActions( tile ) {
        var edges = ['t','r','b','l'];
        for ( var key in edges ) {
            if ( tile[ edges[ key ] ] !== null ) {
                _action( tile[ edges [ key ] ] );
            }
        }
    };

    var tilesAddedThisTurn = function tilesAddedThisTurn() {
        return tilesAddedInCurrentTurn;
    };

    var clearTilesFromThisTurn = function clearTilesFromThisTurn() {
        tilesAddedInCurrentTurn = [];
    };

    return {
        possibleMoves: possibleMoves,
        tiles: _tiles,
        possibleJungleMoves: possibleJungleMoves,
        addVilligeTile: addVilligeTile,
        addJungleTile: addJungleTile,
        players: players,
        addTile: addTile,
        reveleadTiles: reveleadTiles,
        jungleTilesHand: jungleTilesHand,
        pickFromHand: pickFromHand,
        removeTileFromHand: removeTileFromHand,
        tilesAddedThisTurn: tilesAddedThisTurn,
        clearTilesFromThisTurn: clearTilesFromThisTurn
    };

};
